package com.portfolio.ianroberts.kotlintest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.portfolio.ianroberts.kotlintest.adapters.DataAdapter
import com.portfolio.ianroberts.kotlintest.models.Person
import kotlinx.android.synthetic.main.content_main.*

/**
 * Created by ianro on 23/04/2018 for KotlinTest.
 */

class HomeActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rvPersonList = findViewById<RecyclerView>(R.id.rvPersonList)
        val rvAdapter = DataAdapter(createFakeData())

        rvAdapter.notifyDataSetChanged()
        rvPersonList.adapter = rvAdapter
        rvPersonList.layoutParams = RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
        rvPersonList.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        rvPersonList.itemAnimator = DefaultItemAnimator()
        rvPersonList.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
    }


    private fun findViews(): View {
        return rvPersonList
    }

    private fun createFakeData(): ArrayList<Person>{
        val dataArray = ArrayList<Person>()

        for (i in 1..10){
            dataArray.add(Person("Ian", "Roberts", i))
            dataArray.add(Person("Ian", "Roberts", i, "Millers Close"))
            dataArray.add(Person("Ian", "Roberts", i, null))
        }

        return dataArray
    }
}