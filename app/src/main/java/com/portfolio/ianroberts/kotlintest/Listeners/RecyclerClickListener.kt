package com.portfolio.ianroberts.kotlintest.Listeners

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.MotionEvent
import android.view.View
import com.portfolio.ianroberts.kotlintest.adapters.DataAdapter

/**
 * Created by ianro on 11/06/2018 for KotlinTest.
 */
class RecyclerClickListener(context: Context, rvPersonList: RecyclerView) : RecyclerView.OnItemTouchListener, DataAdapter.ClickListener {
    override fun onClick(view: View, position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLongClick(view: View, position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onTouchEvent(rv: RecyclerView?, e: MotionEvent?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onInterceptTouchEvent(rv: RecyclerView?, e: MotionEvent?): Boolean {
        val childView = rv!!.findChildViewUnder(e!!.x, e.y)!!

        //clickListener.onClick(childView, rv.getChildLayoutPosition(childView))

        return false
    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
