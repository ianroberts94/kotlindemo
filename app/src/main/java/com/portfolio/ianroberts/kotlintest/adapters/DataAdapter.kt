package com.portfolio.ianroberts.kotlintest.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.portfolio.ianroberts.kotlintest.R
import com.portfolio.ianroberts.kotlintest.customviews.HelloWorldTextTv
import com.portfolio.ianroberts.kotlintest.models.Person


/**
 * Created by ianro on 24/04/2018 for KotlinTest.
 */
class DataAdapter(var data: ArrayList<Person>?) : RecyclerView.Adapter<DataAdapter.MyViewHolder>() {


    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {

        holder?.tvFullName?.text = StringBuilder()
                .append(data!![position].firstName)
                .append(" ")
                .append(data!![position].lastName)

        holder?.tvAge?.text = data!![position].age.toString()

        holder?.tvAddress?.text = data!![position].address
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): DataAdapter.MyViewHolder {

        val itemView = LayoutInflater.from(parent?.context).inflate(R.layout.layout_person_list_row, parent, false)

        val viewHolder = MyViewHolder(itemView)
        viewHolder.listener = object : ClickListener{
            override fun onClick(view: View, position: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onLongClick(view: View, position: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        }

        return viewHolder
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    class MyViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        var tvFullName: HelloWorldTextTv = view.findViewById(R.id.tvFullName)
        var tvAge: HelloWorldTextTv = view.findViewById(R.id.tvAge)
        var tvAddress: HelloWorldTextTv = view.findViewById(R.id.tvAddress)
        var listener : ClickListener? = null
    }

    interface ClickListener {

        fun onClick(view: View, position: Int)

        fun onLongClick(view: View, position: Int)

    }

}