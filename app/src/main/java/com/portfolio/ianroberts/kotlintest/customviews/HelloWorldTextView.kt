package com.portfolio.ianroberts.kotlintest.customviews

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView

/**
 * Created by ianro on 24/04/2018 for KotlinTest.
 */
class HelloWorldTextTv(context: Context, attribs: AttributeSet) : TextView(context, attribs)