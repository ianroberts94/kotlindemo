package com.portfolio.ianroberts.kotlintest.models

/**
 * Created by ianro on 24/04/2018 for KotlinTest.
 */
class Person(var firstName: String, var lastName: String, var age: Int) {

    constructor(firstName: String, lastName: String, age: Int, address: String?) : this(firstName, lastName, age){
        this.address = address
    }

    var address: String? = null


}